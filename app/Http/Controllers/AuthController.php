<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function formSignUp(){
        return view('form');
    }

    public function submitForm(Request $request){
        $FirstN = $request['fn'];
        $LastN = $request['ln'];
        return view('welcome', compact('FirstN', 'LastN'));
    }

}
